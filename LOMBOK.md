# Introduction
During development of aggregation-service usage of lombok was introduced. To ensure that it will not grow wild we have some decided upon some rules.
The following lombok annotations will be used:

- @RequiredArgsConstructor
- @Getter or @Setter
- @Slf4j

## To be discussed still

No decision has been made for the @Builder annotations.
