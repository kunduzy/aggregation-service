Aggregation Service
---

The API accepts three different parameters to specify the values to be passed to the individual backing APIs(`xyzassessment/backend-services`). 
It returns the consolidated results in a Json object. For cases where the backing api fails to return a good result, due to either error or timeout, the field will still be included in the returned object, but the value will be 'null'.

##### Backend Services
`https://hub.docker.com/r/xyzassessment/backend-services.`

### Package design
The packages of the system use a combination of common design elements mixed with feature based work. Consider
`com.tnt.backendservices.aggregationservice` as the prefix for all packages described below:

* `api` - backend service api client's(Pricing, Shipments, Track)
* `configuration` - configuration files
* `controller` - service controllers
* `domain` - domain classes used by services/features
* `exception` - contains exception and handlers
* `queue` - contains queue implementation classes
* `service` - contains services
* `util` - contains utility classes

### Architectural Decisions
TODO

### Business validations
TODO

### UML
TODO

### Class Diagrams
TODO

### Exceptions
TODO

### Usage
- Ensure you have a local Docker installed
- Run `./mvnw clean install` to build the project.
- To start the application, type `docker-compose up` OR use maven runner `./mvnw spring-boot:run`
- To build docker image manually, type: `docker-compose build` OR `docker build . -t aggregation-service`

Open the following url:
`http://127.0.0.1:8082/aggregation?pricing=NL,CN&track=109347263,123456891&shipments=109347263,123456891`

#### Application properties
* `app.api-client.queue.limit`  - Queue limit for the backed-services API
* `app.api-client.queue.timeout-in-seconds`  - Timeout to handle request immediately, in case of the queue cap for a specific service is not reached
* `app.api-client.pricing`      - Pricing API configurations
* `app.api-client.shipments`    - Shipments API configurations
* `app.api-client.track`        - Track API configurations

### Dependencies
TODO

### Lombok
For usage of lombok see documentation [here](LOMBOK.md)

 
FedEx Assignment 
---
For more information about this assessment see documentation [here](assessment_backend_java.pdf)

### User stories
#### AS-1: As TNT, I want to be able to query all services in a single network call to optimise network traffic.
Implement the interface described above that accepts a collection of API requests to Pricing, Track and/or Shipments. For each request to the aggregation API, the different calls should be forwarded to the individual APIs. Only upon receiving all responses should the complete set of responses be returned to the caller.

#### AS-2: as TNT, I want service calls to be throttled and bulked into consolidated requests per respective API to prevent services from being overloaded.
To prevent overloading the APIs with query calls we would like to consolidate calls per API endpoint. All incoming requests for each individual API should be kept in a queue and be forwarded to the API as soon as a cap of 5 calls for an individual API is reached.
If the cap for a specific API is reached a single request will be sent using the q parameter with 5 comma delimited values.
Example: if there is a caller querying each API and the queue of the Pricing API holds 4 requests, the next request to the Pricing API will trigger the actual bulk request to be made. Each API will have its own queue.
Only upon receiving a response from all API endpoints that were queried should the original service request be responded to.
Out of scope for this story is dealing with calls that remain in the queue due to not submitting exactly multiples of 5 for each API.

#### AS-3: as TNT, I want service calls to be scheduled periodically even if the queue is not full to prevent overly-long response times.
Our current implementation has one major downside; the caller will not receive a response to its requests if the queue cap for a specific service is not reached. To solve this, we want the service queues to also be sent out within 5 seconds of the oldest item being inserted into the queue. In case of the cap being reached within these 5 seconds, the timer should be reset to zero.
This will allow us to meet the 10 second SLA for requests to the aggregation service.

### Assuming that; 
- CI/CD is in place
- This is a backend service
- Authentication/Authorization's done by another service

### TODO
- Add Unit tests to improve code coverage
- Component test, using RestAssured with Cucumber and WireMock to stub backend-Services
- Build retry pattern when api call fails. 
- Use contracts(OpenAPI definition) to generate api objects(request, response, ...).
- Security configuration, spring security filter to verify the request!?
- Update default spring profile for production, override api's url settings, ..., maybe use config server?
- Override logback file for prod
- Improve logging and log-level
- Add Actuator for health check
- Add GraphQL support, that would help consumers to specify the response. 
- OpenFeign for declarative REST Client
