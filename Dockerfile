FROM openjdk:11-jdk-slim

COPY target/aggregation-service.jar aggregation-service.jar
EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/aggregation-service.jar"]
