package com.tnt.backendservices.aggregationservice.test.util;

import com.tnt.backendservices.aggregationservice.domain.dto.AggregationRequest;
import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import com.tnt.backendservices.aggregationservice.domain.model.Shipments;
import com.tnt.backendservices.aggregationservice.domain.model.Track;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class RequestResponseUtil {
    public static final String ITEM_RESULT = "RESULT";
    public static final String ITEM_KEY = "123456789";

    public static AggregationRequest aggregationRequest() {
        final AggregationRequest request = new AggregationRequest();
        request.setPricing(buildRequest());
        request.setTrack(buildRequest());
        request.setShipments(buildRequest());
        return request;
    }

    public static Set<String> buildRequest() {
        final Set<String> set = new HashSet<>();
        set.add(ITEM_KEY);
        return set;
    }

    public static Set<String> buildRequest(final String key) {
        final Set<String> set = new HashSet<>();
        set.add(key);
        return set;
    }

    public static Pricing buildPricingResponse() {
        final Pricing pricing = new Pricing();
        buildRequest().forEach(s -> pricing.put(s, ITEM_RESULT));
        return pricing;
    }

    public static Pricing buildPricingResponse(final String key) {
        final Pricing pricing = new Pricing();
        buildRequest(key).forEach(s -> pricing.put(s, ITEM_RESULT));
        return pricing;
    }

    public static Track buildTrackResponse() {
        final Track track = new Track();
        buildRequest().forEach(s -> track.put(s, ITEM_RESULT));
        return track;
    }

    public static Track buildTrackResponse(final String key) {
        final Track track = new Track();
        buildRequest(key).forEach(s -> track.put(s, ITEM_RESULT));
        return track;
    }

    public static Shipments buildShipmentsResponse() {
        final Shipments shipments = new Shipments();
        buildRequest().forEach(s -> shipments.put(s, Collections.singletonList(ITEM_RESULT)));
        return shipments;
    }
}
