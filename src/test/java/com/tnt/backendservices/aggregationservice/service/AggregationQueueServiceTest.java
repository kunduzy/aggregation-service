package com.tnt.backendservices.aggregationservice.service;

import com.tnt.backendservices.aggregationservice.api.PricingApi;
import com.tnt.backendservices.aggregationservice.api.ShipmentsApi;
import com.tnt.backendservices.aggregationservice.api.TrackApi;
import com.tnt.backendservices.aggregationservice.configuration.ApplicationProperties;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationRequest;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationResponse;
import com.tnt.backendservices.aggregationservice.queue.handler.PricingDataLoader;
import com.tnt.backendservices.aggregationservice.queue.handler.ShipmentsDataLoader;
import com.tnt.backendservices.aggregationservice.queue.handler.TrackDataLoader;
import com.tnt.backendservices.aggregationservice.util.RandomUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.ITEM_KEY;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.ITEM_RESULT;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.aggregationRequest;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildPricingResponse;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildShipmentsResponse;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildTrackResponse;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.just;

@ExtendWith(SpringExtension.class)
class AggregationQueueServiceTest {

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private PricingApi pricingApi;
    @Mock
    private TrackApi trackApi;
    @Mock
    private ShipmentsApi shipmentsApi;

    private AggregationQueueService aggregationQueueService;

    @BeforeEach
    void init() {
        final ApplicationProperties.ApiClient.Queue queue = new ApplicationProperties.ApiClient.Queue();
        queue.setLimit(1);
        final ApplicationProperties.ApiClient apiClient = new ApplicationProperties.ApiClient();
        apiClient.setQueue(queue);

        when(this.applicationProperties.getApiClient()).thenReturn(apiClient);
        when(this.pricingApi.fetch(ArgumentMatchers.anySet()))
                .thenReturn(just(buildPricingResponse()));
        when(this.shipmentsApi.fetch(ArgumentMatchers.anySet()))
                .thenReturn(just(buildShipmentsResponse()));
        when(this.trackApi.fetch(ArgumentMatchers.anySet()))
                .thenReturn(just(buildTrackResponse()));

        final PricingDataLoader pricingDataLoader = new PricingDataLoader(this.pricingApi, this.applicationProperties);
        final ShipmentsDataLoader shipmentsDataLoader = new ShipmentsDataLoader(this.shipmentsApi, this.applicationProperties);
        final TrackDataLoader trackDataLoader = new TrackDataLoader(this.trackApi, this.applicationProperties);
        this.aggregationQueueService = new AggregationQueueService(pricingDataLoader, trackDataLoader, shipmentsDataLoader);
    }

    @Test
    void test() {
        final String key = RandomUtil.nextEncodedString();
        final AggregationRequest request = aggregationRequest();
        final AggregationResponse response = this.aggregationQueueService.handle(key, request);
        assertAll(
                () -> assertEquals(request.getPricing().size(), response.getPricing().size()),
                () -> assertEquals(ITEM_RESULT, response.getPricing().get(ITEM_KEY)),
                () -> assertEquals(ITEM_RESULT, response.getTrack().get(ITEM_KEY)),
                () -> assertEquals(ITEM_RESULT, response.getShipments().get(ITEM_KEY).get(0))
        );
    }

}
