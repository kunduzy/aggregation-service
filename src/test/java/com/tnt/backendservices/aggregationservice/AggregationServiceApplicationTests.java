package com.tnt.backendservices.aggregationservice;

import com.tnt.backendservices.aggregationservice.api.PricingApi;
import com.tnt.backendservices.aggregationservice.api.ShipmentsApi;
import com.tnt.backendservices.aggregationservice.api.TrackApi;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationRequest;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationResponse;
import com.tnt.backendservices.aggregationservice.service.AggregationQueueService;
import com.tnt.backendservices.aggregationservice.util.RandomUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.stream.IntStream;

import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.ITEM_KEY;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.ITEM_RESULT;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.aggregationRequest;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildPricingResponse;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildShipmentsResponse;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildTrackResponse;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.just;

@SpringBootTest
@ActiveProfiles("test")
class AggregationServiceApplicationTests {
    @MockBean
    private PricingApi pricingApi;
    @MockBean
    private ShipmentsApi shipmentsApi;
    @MockBean
    private TrackApi trackApi;

    @Autowired
    private AggregationQueueService aggregationQueueService;

    @BeforeEach
    void init() {
        when(this.pricingApi.fetch(ArgumentMatchers.anySet()))
                .thenReturn(just(buildPricingResponse()));
        when(this.shipmentsApi.fetch(ArgumentMatchers.anySet()))
                .thenReturn(just(buildShipmentsResponse()));
        when(this.trackApi.fetch(ArgumentMatchers.anySet()))
                .thenReturn(just(buildTrackResponse()));
    }

    @Test
    @DisplayName("Service will return response within 1 second, in case of the cap being reached")
    @Tag("LoadTest")
    void callerWillReceiveResponse() {
        IntStream.range(0, 50)
                .parallel().forEach(integer -> {
            final Instant timeout = Instant.now().plusSeconds(1);
            final String key = RandomUtil.nextEncodedString();
            final AggregationRequest request = aggregationRequest();
            final AggregationResponse response = this.aggregationQueueService.handle(key, request);
            assertAll(
                    () -> assertEquals(request.getPricing().size(), response.getPricing().size()),
                    () -> assertEquals(ITEM_RESULT, response.getPricing().get(ITEM_KEY)),
                    () -> assertEquals(ITEM_RESULT, response.getTrack().get(ITEM_KEY)),
                    () -> assertEquals(ITEM_RESULT, response.getShipments().get(ITEM_KEY).get(0)),
                    () -> assertTrue(Instant.now().isBefore(timeout))
            );
        });
    }

    @Test
    @DisplayName("Response will be sent out within 6 seconds")
    void responseSentOutWithinConfiguredTimeout() {
        final Instant timeout = Instant.now().plusSeconds(6);
        final String key = RandomUtil.nextEncodedString();
        final AggregationRequest request = aggregationRequest();
        final AggregationResponse response = this.aggregationQueueService.handle(key, request);
        assertAll(
                () -> assertEquals(request.getPricing().size(), response.getPricing().size()),
                () -> assertEquals(ITEM_RESULT, response.getPricing().get(ITEM_KEY)),
                () -> assertEquals(ITEM_RESULT, response.getTrack().get(ITEM_KEY)),
                () -> assertEquals(ITEM_RESULT, response.getShipments().get(ITEM_KEY).get(0)),
                () -> assertTrue(Instant.now().isBefore(timeout))
        );
    }
}
