package com.tnt.backendservices.aggregationservice.queue.handler;

import com.tnt.backendservices.aggregationservice.api.PricingApi;
import com.tnt.backendservices.aggregationservice.configuration.ApplicationProperties;
import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.MessageStatus;
import com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil;
import com.tnt.backendservices.aggregationservice.util.RandomUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildPricingResponse;
import static com.tnt.backendservices.aggregationservice.test.util.RequestResponseUtil.buildRequest;
import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.just;

@ExtendWith(MockitoExtension.class)
class PricingDataLoaderTest {

    @Mock
    private PricingApi api;
    @Mock
    private ApplicationProperties properties;

    @Test
    void whenQueueCapReached() throws InterruptedException {
        final String requestKey = RandomUtil.nextEncodedString();
        final Set<String> request = buildRequest(requestKey);
        mockApi(requestKey);
        mockProperties(1);

        final PricingDataLoader pricingDataLoader = new PricingDataLoader(this.api, this.properties);
        pricingDataLoader.getQueue().clear();
        pricingDataLoader.offer(requestKey, request);
        sleep(1000);
        final Message<Pricing> message = pricingDataLoader.getResponses().get(requestKey);
        assertAll(
                () -> assertEquals(MessageStatus.PROCESSED, message.getStatus()),
                () -> assertEquals(requestKey, message.getKey()),
                () -> assertEquals(RequestResponseUtil.ITEM_RESULT, message.getResult().get(requestKey))
        );
    }


    @Test
    void whenRequestIsEmpty() {
        final String requestKey = RandomUtil.nextEncodedString();
        final Set<String> request = new HashSet<>();
        mockProperties(1);
        final PricingDataLoader pricingDataLoader = new PricingDataLoader(this.api, this.properties);
        pricingDataLoader.getQueue().clear();
        pricingDataLoader.offer(requestKey, request);

        final Message<Pricing> message = pricingDataLoader.getResponses().get(requestKey);
        assertAll(
                () -> assertEquals(MessageStatus.PROCESSED, message.getStatus()),
                () -> assertEquals(requestKey, message.getKey()),
                () -> assertNull(message.getResult().get(requestKey))
        );
    }

    @Test
    void whenQueueCapNotReached() {
        final String requestKey = RandomUtil.nextEncodedString();
        final Set<String> request = buildRequest(requestKey);
        mockProperties(2);
        final PricingDataLoader pricingDataLoader = new PricingDataLoader(this.api, this.properties);
        pricingDataLoader.getQueue().clear();
        pricingDataLoader.offer(requestKey, request);

        final Message<Pricing> message = pricingDataLoader.getResponses().get(requestKey);
        assertAll(
                () -> assertNull(message)
        );
    }

    private void mockApi(final String requestKey) {
        when(this.api.fetch(ArgumentMatchers.anySet()))
                .thenReturn(just(buildPricingResponse(requestKey)));
    }

    private void mockProperties(final int queueLimit) {
        final ApplicationProperties.ApiClient.Queue queue = new ApplicationProperties.ApiClient.Queue();
        queue.setTimeoutInSeconds(5);
        queue.setLimit(queueLimit);
        final ApplicationProperties.ApiClient apiClient = new ApplicationProperties.ApiClient();
        apiClient.setQueue(queue);
        when(this.properties.getApiClient()).thenReturn(apiClient);
    }
}
