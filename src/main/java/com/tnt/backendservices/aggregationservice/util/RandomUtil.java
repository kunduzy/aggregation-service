package com.tnt.backendservices.aggregationservice.util;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;

public final class RandomUtil {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private static final Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();

    private RandomUtil() {
        throw new UnsupportedOperationException();
    }

    public static String nextEncodedString() {
        //return String.valueOf(SECURE_RANDOM.nextLong());
        return new String(encoder.encode(nextBytes(20)), StandardCharsets.UTF_8);
    }

    private static byte[] nextBytes(final int length) {
        final byte[] randomBytes = new byte[length];
        SECURE_RANDOM.nextBytes(randomBytes);
        return randomBytes;
    }
}
