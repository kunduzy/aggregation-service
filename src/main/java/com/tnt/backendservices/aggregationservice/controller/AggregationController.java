package com.tnt.backendservices.aggregationservice.controller;

import com.tnt.backendservices.aggregationservice.domain.dto.AggregationRequest;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationResponse;
import com.tnt.backendservices.aggregationservice.service.AggregationQueueService;
import com.tnt.backendservices.aggregationservice.util.RandomUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("aggregation")
@AllArgsConstructor
@Slf4j
public class AggregationController {
    private final AggregationQueueService aggregationService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public AggregationResponse aggregation(@Valid final AggregationRequest aggregationRequest) {
        log.info("Incoming request {}", aggregationRequest);
        return this.aggregationService.handle(RandomUtil.nextEncodedString(), aggregationRequest);
    }
}

