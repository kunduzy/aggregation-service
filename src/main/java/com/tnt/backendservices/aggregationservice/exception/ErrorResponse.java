package com.tnt.backendservices.aggregationservice.exception;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ErrorResponse {
    private final ErrorCode code;
    private final String message;

    public Integer getCode() {
        return this.code.getCode();
    }
}
