package com.tnt.backendservices.aggregationservice.exception;

public enum ErrorCode {
    SERVICE_ERROR(400),
    SYSTEM_ERROR(500);

    private final Integer code;

    ErrorCode(final Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }

}
