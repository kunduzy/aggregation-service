package com.tnt.backendservices.aggregationservice.queue.handler;

import com.tnt.backendservices.aggregationservice.api.PricingApi;
import com.tnt.backendservices.aggregationservice.configuration.ApplicationProperties;
import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.consumer.PricingConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
@Service
public class PricingDataLoader extends AbstractDataLoader<Pricing> {
    private static final BlockingQueue<Message<Pricing>> queue = new LinkedBlockingQueue<>();
    private static final Map<String, Message<Pricing>> responses = new ConcurrentHashMap<>();

    public PricingDataLoader(final PricingApi api, final ApplicationProperties properties) {
        super(queue, responses, new PricingConsumer(queue, responses, api, properties.getApiClient().getQueue().getLimit()), properties);
    }

    @Override
    public Pricing getResponseType() {
        return new Pricing();
    }
}
