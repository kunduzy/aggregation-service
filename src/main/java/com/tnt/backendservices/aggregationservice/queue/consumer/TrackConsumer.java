package com.tnt.backendservices.aggregationservice.queue.consumer;

import com.tnt.backendservices.aggregationservice.api.BackendServiceApi;
import com.tnt.backendservices.aggregationservice.domain.model.Track;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.MessageStatus;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

@Slf4j
public class TrackConsumer extends AbstractConsumer<Track> {

    public TrackConsumer(final BlockingQueue<Message<Track>> queue,
                         final Map<String, Message<Track>> responses,
                         final BackendServiceApi<Track> api, final Integer queueLimit) {
        super(queue, responses, api, queueLimit);
    }

    @Override
    void updateResponses(final Track item) {
        log.debug("Track api result {}", item);
        this.getResponses().forEach((key, message) -> {
            message.setStatus(MessageStatus.PROCESSED);
            message.getValue().forEach(s -> {
                message.getResult().put(s, item.get(s));
                message.getResult().put(s, item.get(s));
            });
        });
        log.debug("Track responses {}", this.getResponses());
    }
}
