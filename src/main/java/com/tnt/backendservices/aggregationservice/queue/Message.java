package com.tnt.backendservices.aggregationservice.queue;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
public class Message<T> {
    /**
     * Unique key per request
     */
    private String key;
    /**
     * Message status
     */
    private MessageStatus status = MessageStatus.NEW;
    /**
     * Contains request params
     */
    private Set<String> value;
    /**
     * Contains result from backend-services
     */
    private T result;

}
