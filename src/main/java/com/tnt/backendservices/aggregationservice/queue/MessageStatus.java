package com.tnt.backendservices.aggregationservice.queue;

public enum MessageStatus {
    NEW, PROCESSING, PROCESSED
}
