package com.tnt.backendservices.aggregationservice.queue.consumer;

import com.tnt.backendservices.aggregationservice.api.BackendServiceApi;
import com.tnt.backendservices.aggregationservice.exception.ApiClientException;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.MessageStatus;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

@Slf4j
public abstract class AbstractConsumer<T> implements Consumer {
    private final int queueLimit;
    private final BlockingQueue<Message<T>> queue;
    private final Map<String, Message<T>> responses;
    private CountDownLatch latch;
    private final BackendServiceApi<T> backendServiceApi;

    public AbstractConsumer(final BlockingQueue<Message<T>> queue,
                            final Map<String, Message<T>> responses,
                            final BackendServiceApi<T> api, final Integer queueLimit) {
        this.queue = queue;
        this.responses = responses;
        this.backendServiceApi = api;
        this.queueLimit = queueLimit;
        this.latch = new CountDownLatch(queueLimit);
    }

    abstract void updateResponses(final T item);

    @SneakyThrows
    @Override
    public void run() {
        synchronized (this.queue) {
            consume();
            log.debug("Notifying queue");
            this.queue.notify();
        }
    }

    @Override
    public void consume() throws InterruptedException {
        log.info("Queue size: {}", this.queue.size());
        if (!this.queue.isEmpty()) {
            setupLatch();
            fetchApi(collectValues());
        }
    }

    public Map<String, Message<T>> getResponses() {
        return this.responses;
    }

    private void setupLatch() {
        if (this.queue.size() >= this.queueLimit) {
            this.latch = new CountDownLatch(this.queueLimit);
        } else {
            this.latch = new CountDownLatch(this.queue.size());
        }
    }

    private Set<String> collectValues() throws InterruptedException {
        final Set<String> values = new HashSet<>();
        while (this.latch.getCount() != 0) {
            this.latch.countDown();

            log.info("Collecting values from queue latch: {}", this.latch.getCount());
            final Message<T> message = this.queue.take();
            log.debug("Message item {}", message);

            handleProcessingState(message);
            values.addAll(message.getValue());
        }
        log.info("Items from queue {}", values);
        return values;
    }

    private void handleProcessingState(final Message<T> message) {
        message.setStatus(MessageStatus.PROCESSING);
        this.responses.put(message.getKey(), message);
    }

    private void fetchApi(final Set<String> items) {
        try {
            this.backendServiceApi.fetch(items)
                    .subscribe(this::updateResponses);
        } catch (final Exception e) {
            throw new ApiClientException("Exception occurred while calling backend api", e);
        }
    }


}
