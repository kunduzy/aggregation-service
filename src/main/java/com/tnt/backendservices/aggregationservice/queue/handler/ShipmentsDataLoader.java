package com.tnt.backendservices.aggregationservice.queue.handler;

import com.tnt.backendservices.aggregationservice.api.ShipmentsApi;
import com.tnt.backendservices.aggregationservice.configuration.ApplicationProperties;
import com.tnt.backendservices.aggregationservice.domain.model.Shipments;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.consumer.ShipmentsConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
@Service
public class ShipmentsDataLoader extends AbstractDataLoader<Shipments> {
    private static final BlockingQueue<Message<Shipments>> queue = new LinkedBlockingQueue<>();
    private static final Map<String, Message<Shipments>> responses = new ConcurrentHashMap<>();

    public ShipmentsDataLoader(final ShipmentsApi shipmentsApi, final ApplicationProperties properties) {
        super(queue, responses, new ShipmentsConsumer(queue, responses, shipmentsApi, properties.getApiClient().getQueue().getLimit()), properties);
    }

    @Override
    public Shipments getResponseType() {
        return new Shipments();
    }
}
