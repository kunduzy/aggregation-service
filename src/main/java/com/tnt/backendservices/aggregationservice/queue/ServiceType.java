package com.tnt.backendservices.aggregationservice.queue;

public enum ServiceType {
    PRICING, SHIPMENTS, TRACK
}
