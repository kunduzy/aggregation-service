package com.tnt.backendservices.aggregationservice.queue.consumer;

import com.tnt.backendservices.aggregationservice.api.BackendServiceApi;
import com.tnt.backendservices.aggregationservice.domain.model.Shipments;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.MessageStatus;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

@Slf4j
public class ShipmentsConsumer extends AbstractConsumer<Shipments> {

    public ShipmentsConsumer(final BlockingQueue<Message<Shipments>> queue,
                             final Map<String, Message<Shipments>> responses,
                             final BackendServiceApi<Shipments> api, final Integer queueLimit) {
        super(queue, responses, api, queueLimit);
    }

    @Override
    void updateResponses(final Shipments item) {
        log.debug("Shipments api result {}", item);
        this.getResponses().forEach((key, message) -> {
            message.setStatus(MessageStatus.PROCESSED);
            message.getValue().forEach(s -> {
                message.getResult().put(s, item.get(s));
                message.getResult().put(s, item.get(s));
            });
        });
        log.debug("Shipments responses {}", this.getResponses());
    }
}
