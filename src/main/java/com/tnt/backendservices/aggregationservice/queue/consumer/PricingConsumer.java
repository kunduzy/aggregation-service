package com.tnt.backendservices.aggregationservice.queue.consumer;

import com.tnt.backendservices.aggregationservice.api.BackendServiceApi;
import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.MessageStatus;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

@Slf4j
public class PricingConsumer extends AbstractConsumer<Pricing> {

    public PricingConsumer(final BlockingQueue<Message<Pricing>> queue,
                           final Map<String, Message<Pricing>> responses,
                           final BackendServiceApi<Pricing> api, final Integer queueLimit) {
        super(queue, responses, api, queueLimit);
    }

    @Override
    void updateResponses(final Pricing item) {
        log.debug("Pricing api result {}", item);
        this.getResponses().forEach((key, message) -> {
            message.setStatus(MessageStatus.PROCESSED);
            message.getValue().forEach(s -> {
                message.getResult().put(s, item.get(s));
                message.getResult().put(s, item.get(s));
            });
        });
        log.debug("Pricing responses {}", this.getResponses());
    }
}
