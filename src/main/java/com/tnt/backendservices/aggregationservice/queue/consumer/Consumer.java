package com.tnt.backendservices.aggregationservice.queue.consumer;

public interface Consumer extends Runnable {
    void consume() throws InterruptedException;
}
