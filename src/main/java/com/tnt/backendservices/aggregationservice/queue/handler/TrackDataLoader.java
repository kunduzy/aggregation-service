package com.tnt.backendservices.aggregationservice.queue.handler;

import com.tnt.backendservices.aggregationservice.api.TrackApi;
import com.tnt.backendservices.aggregationservice.configuration.ApplicationProperties;
import com.tnt.backendservices.aggregationservice.domain.model.Track;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.consumer.TrackConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
@Service
public class TrackDataLoader extends AbstractDataLoader<Track> {
    private static final BlockingQueue<Message<Track>> queue = new LinkedBlockingQueue<>();
    private static final Map<String, Message<Track>> responses = new ConcurrentHashMap<>();

    public TrackDataLoader(final TrackApi api, final ApplicationProperties properties) {
        super(queue, responses, new TrackConsumer(queue, responses, api, properties.getApiClient().getQueue().getLimit()), properties);
    }

    @Override
    public Track getResponseType() {
        return new Track();
    }
}
