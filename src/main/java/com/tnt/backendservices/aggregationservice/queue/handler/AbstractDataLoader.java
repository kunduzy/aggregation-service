package com.tnt.backendservices.aggregationservice.queue.handler;

import com.tnt.backendservices.aggregationservice.configuration.ApplicationProperties;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.MessageStatus;
import com.tnt.backendservices.aggregationservice.queue.consumer.Consumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Instant;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@EnableScheduling
public abstract class AbstractDataLoader<T> implements DataLoader<T> {
    private final BlockingQueue<Message<T>> queue;
    private final Map<String, Message<T>> responses;
    final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final Consumer consumer;
    private final ApplicationProperties applicationProperties;
    private Instant lastOfferedTime;

    public AbstractDataLoader(final BlockingQueue<Message<T>> queue,
                              final Map<String, Message<T>> responses, final Consumer consumer, final ApplicationProperties applicationProperties) {
        this.consumer = consumer;
        this.queue = queue;
        this.responses = responses;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void offer(final String key, final Set<String> request) {
        if (isRequestEmpty(request)) {
            this.responses.put(key, buildMessage(key, request, MessageStatus.PROCESSED));
            return;
        }

        this.queue.offer(buildMessage(key, request, MessageStatus.NEW));

        this.lastOfferedTime = Instant.now();
        if (this.queue.size() >= this.applicationProperties.getApiClient().getQueue().getLimit()) {
            consume();
        }
    }

    @Override
    public BlockingQueue<Message<T>> getQueue() {
        return this.queue;
    }

    @Override
    public Map<String, Message<T>> getResponses() {
        return this.responses;
    }

    private boolean isRequestEmpty(final Set<String> request) {
        return (request == null || request.isEmpty());
    }

    private void consume() {
        this.executorService.submit(this.consumer);
    }

    private Message<T> buildMessage(final String key, final Set<String> request, final MessageStatus status) {
        final Message<T> message = new Message<>();
        message.setKey(key);
        message.setValue(request);
        message.setResult(getResponseType());
        message.setStatus(status);
        return message;
    }

    @Scheduled(fixedRate = 100)
    private void checkQueueTimeout() {
        final int timeout = this.applicationProperties.getApiClient().getQueue().getTimeoutInSeconds();
        if (!this.queue.isEmpty() && Instant.now().isAfter(this.lastOfferedTime.plusSeconds(timeout))) {
            log.info("Consuming scheduled queue");
            consume();
        }
    }
}
