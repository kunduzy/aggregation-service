package com.tnt.backendservices.aggregationservice.queue.handler;

import com.tnt.backendservices.aggregationservice.queue.Message;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

public interface DataLoader<T> {
    void offer(final String key, final Set<String> request);

    T getResponseType();

    Map<String, Message<T>> getResponses();

    BlockingQueue<Message<T>> getQueue();
}
