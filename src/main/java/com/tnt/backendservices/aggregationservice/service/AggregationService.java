package com.tnt.backendservices.aggregationservice.service;

import com.tnt.backendservices.aggregationservice.api.PricingApi;
import com.tnt.backendservices.aggregationservice.api.ShipmentsApi;
import com.tnt.backendservices.aggregationservice.api.TrackApi;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationRequest;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationResponse;
import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import com.tnt.backendservices.aggregationservice.domain.model.Shipments;
import com.tnt.backendservices.aggregationservice.domain.model.Track;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.util.function.Tuple3;

import static reactor.core.publisher.Mono.zip;

@Service
@AllArgsConstructor
@Slf4j
public class AggregationService {
    private final PricingApi pricingApi;
    private final ShipmentsApi shipmentsApi;
    private final TrackApi trackApi;

    public AggregationResponse fetchAll(final AggregationRequest aggregationRequest) {
        final Tuple3<Pricing, Track, Shipments> result = zip(this.pricingApi.fetch(aggregationRequest.getPricing()),
                this.trackApi.fetch(aggregationRequest.getTrack()),
                this.shipmentsApi.fetch(aggregationRequest.getShipments()))
                .block();

        return buildResponse(result);
    }

    private AggregationResponse buildResponse(final Tuple3<Pricing, Track, Shipments> result) {
        if (result == null) {
            log.info("Result is null, returning empty response");
            return AggregationResponse.builder()
                    .build();
        }

        return AggregationResponse.builder()
                .pricing(result.getT1())
                .track(result.getT2())
                .shipments(result.getT3())
                .build();
    }

}
