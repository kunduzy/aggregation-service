package com.tnt.backendservices.aggregationservice.service;

import com.tnt.backendservices.aggregationservice.domain.dto.AggregationRequest;
import com.tnt.backendservices.aggregationservice.domain.dto.AggregationResponse;
import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import com.tnt.backendservices.aggregationservice.domain.model.Shipments;
import com.tnt.backendservices.aggregationservice.domain.model.Track;
import com.tnt.backendservices.aggregationservice.queue.Message;
import com.tnt.backendservices.aggregationservice.queue.ServiceType;
import com.tnt.backendservices.aggregationservice.queue.handler.PricingDataLoader;
import com.tnt.backendservices.aggregationservice.queue.handler.ShipmentsDataLoader;
import com.tnt.backendservices.aggregationservice.queue.handler.TrackDataLoader;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.tnt.backendservices.aggregationservice.queue.MessageStatus.PROCESSED;
import static com.tnt.backendservices.aggregationservice.queue.ServiceType.PRICING;
import static com.tnt.backendservices.aggregationservice.queue.ServiceType.SHIPMENTS;
import static com.tnt.backendservices.aggregationservice.queue.ServiceType.TRACK;
import static java.lang.Thread.sleep;

@Service
@AllArgsConstructor
@Slf4j
public class AggregationQueueService {
    private final PricingDataLoader pricingDataLoader;
    private final TrackDataLoader trackDataLoader;
    private final ShipmentsDataLoader shipmentsDataLoader;

    public AggregationResponse handle(final String key, final AggregationRequest request) {

        this.pricingDataLoader.offer(key, request.getPricing());
        this.shipmentsDataLoader.offer(key, request.getShipments());
        this.trackDataLoader.offer(key, request.getTrack());

        try {
            // Block the request until it has been processed (User story AS-2)
            waitUntilDataProcessed(key);
        } catch (final InterruptedException e) {
            Thread.currentThread().interrupt();
            log.warn("Exception occurred while waiting api-client", e);
        }

        return buildResponse(key);
    }

    private AggregationResponse buildResponse(final String key) {
        final Message<Pricing> pricingMessage = this.pricingDataLoader.getResponses().remove(key);
        final Message<Shipments> shipmentsMessage = this.shipmentsDataLoader.getResponses().remove(key);
        final Message<Track> trackMessage = this.trackDataLoader.getResponses().remove(key);
        log.info("Messages pricing: {}, shipments: {}, track: {}", pricingMessage, shipmentsMessage, trackMessage);

        return AggregationResponse.builder()
                .pricing(pricingMessage != null ? pricingMessage.getResult() : null)
                .track(trackMessage != null ? trackMessage.getResult() : null)
                .shipments(shipmentsMessage != null ? shipmentsMessage.getResult() : null)
                .build();
    }

    private synchronized void waitUntilDataProcessed(final String key) throws InterruptedException {
        log.debug("Waiting for key: {}", key);
        final ConcurrentMap<ServiceType, Boolean> found = new ConcurrentHashMap<>();
        while (true) {
            checkPricingData(key, found);
            checkShipmentsData(key, found);
            checkTrackData(key, found);
            sleep(10);
            if (found.size() == 3) {
                break;
            }
        }
        log.info("Data loaded for key:{}", key);
    }

    private void checkTrackData(final String key, final ConcurrentMap<ServiceType, Boolean> found) {
        if (!found.containsKey(TRACK)
                && this.trackDataLoader.getResponses().get(key) != null
                && this.trackDataLoader.getResponses().get(key).getStatus().equals(PROCESSED)) {
            found.put(TRACK, true);
        }
    }

    private void checkShipmentsData(final String key, final ConcurrentMap<ServiceType, Boolean> found) {
        if (!found.containsKey(SHIPMENTS)
                && this.shipmentsDataLoader.getResponses().get(key) != null
                && this.shipmentsDataLoader.getResponses().get(key).getStatus().equals(PROCESSED)) {
            found.put(SHIPMENTS, true);
        }
    }

    private void checkPricingData(final String key, final ConcurrentMap<ServiceType, Boolean> found) {
        if (!found.containsKey(PRICING)
                && this.pricingDataLoader.getResponses().get(key) != null
                && this.pricingDataLoader.getResponses().get(key).getStatus().equals(PROCESSED)) {
            found.put(PRICING, true);
        }
    }
}
