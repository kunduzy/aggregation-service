package com.tnt.backendservices.aggregationservice.domain.dto;

import com.tnt.backendservices.aggregationservice.domain.validation.ValidIsoCountryCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Pattern;
import java.util.Set;

@Getter
@Setter
@ToString
public class AggregationRequest {
    private static final String NOT_VALID = "format is not valid";
    public static final String REGEX_9_DIGITS = "^[0-9]{9}$";
    public static final String REGEX_ISO_COUNTRY = "^[0-9]{9}$";
    private Set<@ValidIsoCountryCode(message = NOT_VALID, optional = true) String> pricing;
    private Set<@Pattern(message = NOT_VALID, regexp = REGEX_9_DIGITS) String> track;
    private Set<@Pattern(message = NOT_VALID, regexp = REGEX_9_DIGITS) String> shipments;
}
