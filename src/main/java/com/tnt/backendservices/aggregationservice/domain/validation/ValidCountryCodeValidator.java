package com.tnt.backendservices.aggregationservice.domain.validation;

import com.google.common.base.Strings;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Locale;

public class ValidCountryCodeValidator implements ConstraintValidator<ValidIsoCountryCode, String> {

    private boolean isOptional;

    @Override
    public void initialize(final ValidIsoCountryCode validIsoCountryCode) {
        this.isOptional = validIsoCountryCode.optional();
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext) {
        final String[] currencies = Locale.getISOCountries();
        final boolean containsIsoCode = Arrays.asList(currencies).contains(value);
        return this.isOptional ? (containsIsoCode || (Strings.isNullOrEmpty(value))) : containsIsoCode;
    }

}
