package com.tnt.backendservices.aggregationservice.domain.dto;

import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import com.tnt.backendservices.aggregationservice.domain.model.Shipments;
import com.tnt.backendservices.aggregationservice.domain.model.Track;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AggregationResponse {
    private final Pricing pricing;
    private final Track track;
    private final Shipments shipments;
}
