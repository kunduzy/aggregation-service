package com.tnt.backendservices.aggregationservice.api;

import reactor.core.publisher.Mono;

import java.util.Set;

public interface BackendServiceApi<T> {
    public static final String DELIMITER = ",";

    Mono<T> fetch(Set<String> items);
}
