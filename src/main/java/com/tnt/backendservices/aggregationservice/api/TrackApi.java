package com.tnt.backendservices.aggregationservice.api;

import com.tnt.backendservices.aggregationservice.domain.model.Track;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Set;

import static reactor.core.publisher.Mono.just;

@Service
@Slf4j
@AllArgsConstructor
public class TrackApi implements BackendServiceApi<Track> {
    private final WebClient trackServiceWebClient;

    @Override
    public Mono<Track> fetch(final Set<String> track) {
        log.info("Track request: {}", track);
        if (track == null || track.isEmpty()) {
            return just(new Track());
        }

        return this.trackServiceWebClient
                .get()
                .uri(uriBuilder -> uriBuilder.build(String.join(DELIMITER, track)))
                .retrieve()
                .bodyToMono(Track.class)
                .onErrorResume(throwable -> onError(track))
                .subscribeOn(Schedulers.boundedElastic());
    }

    private Mono<Track> onError(final Set<String> track) {
        log.warn("Exception occurred while calling Track API, returning empty");
        final Track result = new Track();
        track.forEach(s -> result.put(s, null));
        return just(result);
    }
}
