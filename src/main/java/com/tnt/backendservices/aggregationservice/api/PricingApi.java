package com.tnt.backendservices.aggregationservice.api;

import com.tnt.backendservices.aggregationservice.domain.model.Pricing;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Set;

import static reactor.core.publisher.Mono.just;

@Service
@Slf4j
@AllArgsConstructor
public class PricingApi implements BackendServiceApi<Pricing> {
    private final WebClient pricingServiceWebClient;

    @Override
    public Mono<Pricing> fetch(final Set<String> pricing) {
        log.info("Pricing request: {}", pricing);
        if (pricing == null || pricing.isEmpty()) {
            return just(new Pricing());
        }

        return this.pricingServiceWebClient
                .get()
                .uri(uriBuilder -> uriBuilder.build(String.join(DELIMITER, pricing)))
                .retrieve()
                .bodyToMono(Pricing.class)
                .onErrorResume(throwable -> onError(pricing))
                .subscribeOn(Schedulers.boundedElastic());
    }

    private Mono<Pricing> onError(final Set<String> pricing) {
        log.warn("Exception occurred while calling Pricing API, returning empty");
        final Pricing result = new Pricing();
        pricing.forEach(s -> result.put(s, null));
        return just(result);
    }

}
