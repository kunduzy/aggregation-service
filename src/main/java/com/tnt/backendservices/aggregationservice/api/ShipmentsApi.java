package com.tnt.backendservices.aggregationservice.api;

import com.tnt.backendservices.aggregationservice.domain.model.Shipments;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Set;

import static reactor.core.publisher.Mono.just;

@Service
@Slf4j
@AllArgsConstructor
public class ShipmentsApi implements BackendServiceApi<Shipments> {
    private final WebClient shipmentsServiceWebClient;

    @Override
    public Mono<Shipments> fetch(final Set<String> shipments) {
        log.info("Shipments request: {}", shipments);
        if (shipments == null || shipments.isEmpty()) {
            return just(new Shipments());
        }

        return this.shipmentsServiceWebClient
                .get()
                .uri(uriBuilder -> uriBuilder.build(String.join(DELIMITER, shipments)))
                .retrieve()
                .bodyToMono(Shipments.class)
                .onErrorResume(throwable -> onError(shipments))
                .subscribeOn(Schedulers.boundedElastic());
    }

    private Mono<Shipments> onError(final Set<String> shipments) {
        log.warn("Exception occurred while calling Shipments API, returning empty");
        final Shipments result = new Shipments();
        shipments.forEach(s -> result.put(s, null));
        return just(result);
    }
}
