package com.tnt.backendservices.aggregationservice.configuration;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@AllArgsConstructor
public class WebClientConfig {
    private final ApplicationProperties applicationProperties;

    @Bean
    public WebClient pricingServiceWebClient() {
        return WebClient.builder()
                .baseUrl(this.applicationProperties.getApiClient().getPricing().getUrl())
                .build();
    }

    @Bean
    public WebClient trackServiceWebClient() {
        return WebClient.builder()
                .baseUrl(this.applicationProperties.getApiClient().getTrack().getUrl())
                .build();
    }

    @Bean
    public WebClient shipmentsServiceWebClient() {
        return WebClient.builder()
                .baseUrl(this.applicationProperties.getApiClient().getShipments().getUrl())
                .build();
    }
}
