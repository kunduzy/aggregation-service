package com.tnt.backendservices.aggregationservice.configuration;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app")
@Data
public class ApplicationProperties {
    private ApiClient apiClient;

    @Getter
    @Setter
    public static class ApiClient {
        private Queue queue;
        private Pricing pricing;
        private Shipments shipments;
        private Track track;

        @Getter
        @Setter
        public static class Queue {
            private int limit;
            private int timeoutInSeconds;
        }

        @Getter
        @Setter
        public static class Pricing {
            private String url;
        }

        @Getter
        @Setter
        public static class Shipments {
            private String url;
        }

        @Getter
        @Setter
        public static class Track {
            private String url;
        }
    }
    
}
